# Arreglos: Rotacion a la izquierda

Rotacion a la izquierda es una operacion sobre arreglos que intercambia una posicion a la izquierda cada elemento. Por ejemplo, si se haran `2` rotaciones en un arreglo `[1,2,3,4,5]`, entonces el resultado seria `[3,4,5,1,2]`.

Dado un arreglo `a` de `n` enteros y un numero, `d`, realiza `d` rotaciones a la izquierda en el arreglo. Regresa el arreglo actualizado para ser impreso en una linea de enteros separados por espacios.

## Formato de entrada

La primera linea contiene dos enteros separados por espacios, `n` (el tamaño del arreglo `a`) y `d` (el numero de rotaciones que tienen que ser alcanzadas).
La segunda linea contine un `n` enteros separados por espacios `a[i]`.

## Restricciones

- `1 <= n <= 10^5`
- `1 <= d <= n`
- `1 <= a[i] <= 10^6`

## Formato de salida

Imprimir en una sola linea de `n` enteros separados por espacios, indicando el estado final del arreglo despues de realizar las `d` rotaciones a la izquierda.

## Ejemplo de entrada

```txt
5 4
1 2 3 4 5
```

## Ejemplo de salia

```txt
5 1 2 3 4
```

## Explicacion

Cuando ejecutamos `d = 4` rotaciones a la izquierda, el arreglo es sometido a la siguiente secuencia de cambios:

```txt
  [1,2,3,4,5] -> [2,3,4,5,1] -> [3,4,5,1,2] -> [4,5,1,2,3] -> [5,1,2,3,4]
```

Fuente: [Hackerrank][1]

[1]: https://www.hackerrank.com/challenges/ctci-array-left-rotation/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays
