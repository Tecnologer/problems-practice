# Arreglo 2D - DS

Dado un arreglo (`arr`) 2D de `6x6`:

```txt
1 1 1 0 0 0
0 1 0 0 0 0
1 1 1 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0
```

Definiremos un reloj de arena en `A` como un subconjunto de valores con los indices que caen en este patron en la representacion grafica de `arr`:

```txt
a b c
  d
e f g
```

Hay 16 relojes de arena en `arr` y la suma de un reloj de arena es la suma de los valores que componen el reloj de arena. Calcula la suma por cada reloj de arena en `arr`, despues imprime el valor de la suma mas grande.

Por ejemplo, dado un arreglo 2D:

```txt
-9 -9 -9  1 1 1
 0 -9  0  4 3 2
-9 -9 -9  1 2 3
 0  0  8  6 6 0
 0  0  0 -2 0 0
 0  0  1  2 4 0
```

Calcularemos los siguientes 16 sumatorias de los relojes de arena:

```txt
-63, -34, -9, 12,
-10, 0, 28, 23,
-27, -11, -2, 10,
9, 17, 25, 18
```

Nuestro valor mas grande es `28` del reloj de arena:

```txt
0 4 3
  1
8 6 6
```

## Formato de entrada

Cada una de las `6` lineas de entrada `arr[i]` contiene `6` enteros separados por espacios `arr[i][j]`.

## Condiciones

- `-9 <= arr[i][j] <= 9`
- `0 <= i,j <= 5`

## Formato de salida

Imprimir el valor mas grande de la suma de un reloj de arena encontrado en `arr`.

## Ejemplo de entrada

```txt
1 1 1 0 0 0
0 1 0 0 0 0
1 1 1 0 0 0
0 0 2 4 4 0
0 0 0 2 0 0
0 0 1 2 4 0
```

## Ejemplo de salida

```txt
19
```

## Explicacion

`arr` contiene los siguientes relojes de arena:

```txt
1 1 1   1 1 0   1 0 0   0 0 0
  1       0       0       0
1 1 1   1 1 0   1 0 0   0 0 0

0 1 0   1 0 0   0 0 0   0 0 0
  1       1       0       0
0 0 2   0 2 4   2 4 4   4 4 0

1 1 1   1 1 0   1 0 0   0 0 0
  0       2       4       4
0 0 0   0 0 2   0 2 0   2 0 0

0 0 2   0 2 4   2 4 4   4 4 0
  0       0       2       0
0 0 1   0 1 2   1 2 4   2 4 0
```

El reloj de arena con la mayor suma(19) es :

```txt
2 4 4
  2
1 2 4
```
