# Manipulacion de Arreglos

Iniciando con un arreglo de ceros indexado en 1 (su primer elemento se encuentra en la posicion 1) y una lista de operaciones; para cada operacion agrega el valor de cada elemento del arreglo entre dos indices dados, incluyendo dichos valores. Una vez que todas las operaciones hayan sido ejecutadas, regresa el valor maximo en el arreglo.

Por ejemplo, la longitud del arreglo de ceros `n = 10`. La lista de consultas es como las siguientes:

```txt
a b k
1 5 3
4 8 7
6 9 1
```

Agrega los valores de `k` entre los indices `a` y `b`:

```txt
index->  1 2 3  4  5 6 7 8 9 10
        [0,0,0, 0, 0,0,0,0,0, 0]
        [3,3,3, 3, 3,0,0,0,0, 0]
        [3,3,3,10,10,7,7,7,0, 0]
        [3,3,3,10,10,8,8,8,1, 0]
```

El valor mas grande es `10` despues de que todas las operaciones fueron ejecutadas.

## Formato de entrada

- La primera linea contiene dos enteros separados por espacios `n` (el tamanio del arreglo) y `m` (numero de operaciones).

- Cada siguiente linea despues de `m` contiene tres enteros separados por espacios, `a` (indice izquierdo), `b` (indice derecho) y `k` (sumando).

## Ejemplo de entrada

```txt
5 3
1 2 100
2 5 100
3 4 100
```

## Condiciones

- `3 <= n <= 10^7`
- `1 <= m <= 2*10^5`
- `1 <= a <= b <= n`
- `0 <= k <= 10^9`

## Formato de salida

Un entero con el valor maximo en el arreglo final.

## Ejemplo de salida

```txt
200
```

## Explicacion

- Resultado de la **primera** actualizacion: `[100, 100, 0, 0, 0]`.
- Resultado de la **segunda** actualizacion: `[100, 200, 100, 100, 100]`.
- Resultado de la **tercera** actualizacion: `[100, 200, 200, 200, 100]`.
- El valor de retorno seria `200`.

Fuente: [Hackerrank][1]

[1]: https://www.hackerrank.com/challenges/crush/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays
