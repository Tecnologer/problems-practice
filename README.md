# Problemas de practica

Repositorio con diferentes problemas para practicar y mejorar tus habilidades como desarrollador.

## Indice

- [Arreglos](./arreglos)
  - [Manipulacion de arreglos](./arreglos/manipulacion.md)
  - [Rotacion izquierda](./arreglos/rotacion-izquierda.md)
  - [Arreglo 2D DS](./arreglos/2d-ds.md)
